using NUnit.Framework;

namespace VinSolutionsCodingExam.Test
{
    public class Tests 
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var sentence = "This programmer is smooth"; 
            var s = new SentenceMinimizer(sentence);

           
            Assert.AreEqual(s.ConvertSentence(),"T2s p6r is s3h");
        }

        [Test]
        public void Test2()
        {
            var sentence = "This0programmer0is0smooth";
            var s = new SentenceMinimizer(sentence);


            Assert.AreEqual(s.ConvertSentence(), "T2s0p6r0is0s3h");
        }
    }
}