﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VinSolutionsCodingExam
{
    public class SentenceMinimizer
    {
        public SentenceMinimizer(string sentance)
        {
            this.OriginalSentence = sentance;
        }

        private string ConvertedSentence { get; set; }

        string OriginalSentence
        {
            get
            {
                return Sentence_Char != null ? new string(Sentence_Char) : String.Empty;
            }
            set
            {
                Sentence_Char = String.IsNullOrEmpty(value) ? new char[0] : value.ToCharArray();
            }
        }

        char[] Sentence_Char { get; set; } 

        public string ConvertSentence()
        {
            if (String.IsNullOrEmpty(ConvertedSentence) && !String.IsNullOrEmpty(OriginalSentence))
            {
                MinimizeString();
            }
            return ConvertedSentence;
        }


        //This method will return a dictionary of idexes for begining and ending of each word.
        protected Dictionary<int, int> SetIndexes()
        {
            var wordBeginEndPositions = new Dictionary<int, int>();
            bool inWord = false;
            int? startOfWord = null;
            for (int currentLetter = 0; currentLetter < Sentence_Char.Length; currentLetter++)
            {
                bool wordBreakChar = !Char.IsLetter(Sentence_Char[currentLetter]);
                if (wordBreakChar && inWord)
                {
                    wordBeginEndPositions.Add((int)startOfWord, currentLetter - 1);
                    inWord = false;
                }
                else if (!wordBreakChar && !inWord)
                {
                    startOfWord = currentLetter;
                    inWord = true;
                }
            }
            if (inWord)
            {
                //add last word indexes
                wordBeginEndPositions.Add((int)startOfWord, Sentence_Char.Length - 1);
            }
            return wordBeginEndPositions;
        }

        //This method will for loop through the original sentence passed in and build a string with minimized words.
        private void MinimizeString()
        {
            var builder = new StringBuilder();
             var wordBeginEndPositions = SetIndexes();
            for (int currentLetter = 0; currentLetter < Sentence_Char.Length; currentLetter++)
            {
                if (wordBeginEndPositions.ContainsKey(currentLetter))
                {
                    int wordCountSpace = wordBeginEndPositions[currentLetter] - currentLetter;
                    if (wordCountSpace > 0)
                    {
                        //Need to get the count of space in between letters by subtracting one from word count. For example, "The" has 1 letter in the middle but has 0 space count in between since its only one.
                        var spaceCountInBetween = wordCountSpace - 1;
                        var lettersInBetween = this.OriginalSentence.Substring(currentLetter + 1, spaceCountInBetween); 
                        var distinctCount = lettersInBetween.Distinct();
                        //If spaceCountInBetween is 0 then dont add the distinct count letter. For example, "Is" will remain "Is"
                        var shortenedWord = spaceCountInBetween > 0 ?  Sentence_Char[currentLetter] + distinctCount.Count().ToString() + Sentence_Char[wordBeginEndPositions[currentLetter]] 
                            : Sentence_Char[currentLetter].ToString() + Sentence_Char[wordBeginEndPositions[currentLetter]].ToString(); 
                        builder.Append(shortenedWord);
                        //move onto the next letter
                        currentLetter = wordBeginEndPositions[currentLetter];
                    }
                    else
                    {
                        //This condition is met when the word is a single char with no word count space.. For example "A" 
                         builder.Append(Sentence_Char[currentLetter]);
                    }
                }
                else
                {
                    //This condition is met when character is a non-alphanumeric char
                    builder.Append(Sentence_Char[currentLetter]);
                }
            }
            ConvertedSentence = builder.ToString();
         }

         
   

    }
}
