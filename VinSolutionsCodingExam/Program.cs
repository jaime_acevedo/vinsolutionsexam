﻿using System;

namespace VinSolutionsCodingExam
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a sentence to convert");

            SentenceMinimizer sentence = new SentenceMinimizer(Console.ReadLine());
             
            Console.WriteLine(sentence.ConvertSentence());
        }
    }
}
